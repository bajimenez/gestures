//
//  SwipeViewController.swift
//  Gestures
//
//  Created by Brayan Jimenez on 17/1/18.
//  Copyright © 2018 Brayan Jimenez. All rights reserved.
//

import UIKit

class SwipeViewController: UIViewController {

    @IBOutlet weak var customView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func SwipeRight(_ sender: Any) {
        let action = sender as! UISwipeGestureRecognizer
        customView.backgroundColor = .green
    }
    
    @IBAction func SwipeLeft(_ sender: Any) {
        let action = sender as! UISwipeGestureRecognizer
        
        customView.backgroundColor = .red
    }
    
    @IBAction func SwipeUp(_ sender: Any) {
       
        let action = sender as! UISwipeGestureRecognizer
        
        customView.backgroundColor = .black
    }
    
    @IBAction func SwipeDown(_ sender: Any) {
        let action = sender as! UISwipeGestureRecognizer
    customView.backgroundColor = .blue
    }
}
