//
//  PinchViewController.swift
//  Gestures
//
//  Created by Brayan Jimenez on 23/1/18.
//  Copyright © 2018 Brayan Jimenez. All rights reserved.
//

import UIKit

class PinchViewController: UIViewController {

    @IBOutlet weak var imageOutlet: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
imageOutlet.isUserInteractionEnabled = true
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchGesture))
        imageOutlet.addGestureRecognizer(pinchGesture)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @objc func pinchGesture(sender:UIPinchGestureRecognizer) {
        sender.view?.transform = (	sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale))!
        sender.scale = 1.0
    }

}

