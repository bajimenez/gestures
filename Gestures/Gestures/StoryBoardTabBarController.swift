//
//  StoryBoardTabBarController.swift
//  Gestures
//
//  Created by Brayan Jimenez on 23/1/18.
//  Copyright © 2018 Brayan Jimenez. All rights reserved.
//

import UIKit

class StoryBoardTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        let left = tabBar.items![0]
        let right = tabBar.items![2]
        let center = tabBar.items![1]
        
        
        
        
        left.title = "Tap"
        right.title = "Swipe"
        center.title = "Pinch"
        
        left.badgeColor = .red
        
        
    }
    


}
